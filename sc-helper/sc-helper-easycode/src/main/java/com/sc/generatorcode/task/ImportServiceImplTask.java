package com.sc.generatorcode.task;

import com.sc.generatorcode.task.base.AbstractTask;
import com.sc.generatorcode.utils.ConfigUtil;
import com.sc.generatorcode.utils.FileUtil;
import com.sc.generatorcode.utils.FreemarkerConfigUtils;
import com.sc.generatorcode.utils.StringUtil;
import freemarker.template.TemplateException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author ：wust
 * @date：2020-06-30
 */
public class ImportServiceImplTask extends AbstractTask {

    public ImportServiceImplTask(String className) {
        super(className);
    }

    @Override
    public void run() throws IOException, TemplateException {
        String name = super.getName()[1];
        String entityPackageName = ConfigUtil.getConfiguration().getBasePackage().getEntityPackageName() + "." + name;


        // 生成Service填充数据
        Map<String, String> data = new HashMap<>();
        data.put("BasePackageName", ConfigUtil.getConfiguration().getBasePackage().getBase());
        data.put("DaoPackageName", ConfigUtil.getConfiguration().getBasePackage().getDao());
        data.put("ServicePackageName", ConfigUtil.getConfiguration().getBasePackage().getService());
        data.put("EntityPackageName", entityPackageName);
        data.put("Author", ConfigUtil.getConfiguration().getAuthor());
        data.put("DateTime", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
        data.put("ClassName", className);
        data.put("EntityName", className);
        data.put("EntityNameForCamelCase", StringUtil.firstToLowerCase(className));


        data.put("Override", "\n    @Override");

        String xmlName = ConfigUtil.getConfiguration().getServerName().concat("-").concat(name).replaceAll("-","_");
        data.put("xmlName", xmlName);

        data.put("beanName",StringUtil.columnName2PropertyName(xmlName).concat("ImportServiceImpl"));

        String fileName = className + "ImportServiceImpl.java";

        String filePath = FileUtil.getSourcePath() + StringUtil.package2Path(ConfigUtil.getConfiguration().getBasePackage().getService());
        FileUtil.generateFile(FreemarkerConfigUtils.TYPE_IMPORT_SERVICE_IMPL, data, filePath,fileName);
    }
}
