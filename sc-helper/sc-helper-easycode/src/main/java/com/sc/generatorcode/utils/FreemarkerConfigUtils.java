package com.sc.generatorcode.utils;

import freemarker.template.Configuration;

import java.io.File;
import java.io.IOException;
import java.util.Locale;

public class FreemarkerConfigUtils {
    private static String path = new File(FreemarkerConfigUtils.class.getClassLoader().getResource("ftls").getFile()).getPath();
    public final static int TYPE_ENTITY = 0;
    public final static int TYPE_ENTITY_LIST = 7;
    public final static int TYPE_ENTITY_SEARCH = 9;
    public final static int TYPE_ENTITY_IMPORT = 10;
    public final static int TYPE_DAO = 1;
    public final static int TYPE_SERVICE = 2;
    public final static int TYPE_CONTROLLER = 3;
    public final static int TYPE_MAPPER = 4;
    public final static int TYPE_INTERFACE = 5;
    public final static int TYPE_VUE_LIST = 11;
    public final static int TYPE_VUE_CREATE = 13;
    public final static int TYPE_VUE_UPDATE = 15;
    public final static int TYPE_VUE_SEARCH_BAR = 17;
    public final static int TYPE_VUE_IMPORT = 19;
    public final static int TYPE_IMPORT_EXCEL_XML = 21;
    public final static int TYPE_EXPORT_EXCEL_XML = 23;
    public final static int TYPE_IMPORT_SERVICE = 25;
    public final static int TYPE_IMPORT_SERVICE_IMPL = 27;


    private static Configuration configuration;

    public static synchronized Configuration getInstance() {
        if (null == configuration) {
            configuration = new Configuration();
            try {
                if (path.contains("jar")){
                    configuration.setClassForTemplateLoading(FreemarkerConfigUtils.class, "/ftls");
                } else {
                    configuration.setDirectoryForTemplateLoading(new File(path));
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            configuration.setEncoding(Locale.CHINA, "utf-8");
        }
        return configuration;
    }
}
