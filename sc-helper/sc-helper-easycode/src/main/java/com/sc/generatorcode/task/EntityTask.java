package com.sc.generatorcode.task;

import com.sc.generatorcode.entity.ColumnInfo;
import com.sc.generatorcode.task.base.AbstractTask;
import com.sc.generatorcode.utils.*;
import com.sc.generatorcode.utils.*;
import freemarker.template.TemplateException;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author ：wust
 * @date：2019-12-26
 */
public class EntityTask extends AbstractTask {

    public EntityTask(String className, List<ColumnInfo> infos) {
        super(className, infos);
    }

    public EntityTask(String tableName,String className, List<ColumnInfo> infos) {
        super(tableName,className, null, null, infos);
    }


    @Override
    public void run() throws IOException, TemplateException {
        String name = super.getName()[1];
        String entityPackageName = ConfigUtil.getConfiguration().getBasePackage().getEntityPackageName() + "." + name;
        String rootDir = System.getProperty("user.dir");
        String entityDir = rootDir + ConfigUtil.getConfiguration().getPath().getEntityDir() + "src\\main\\java\\" + StringUtil.package2Path(entityPackageName);
        FileUtil.mkdirs(entityDir);

        Map<String, String> data = new HashMap<>();
        data.put("BasePackageName", ConfigUtil.getConfiguration().getBasePackage().getBase());
        data.put("EntityPackageName", entityPackageName);
        data.put("Author", ConfigUtil.getConfiguration().getAuthor());
        data.put("DateTime", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
        data.put("ClassName", className);
        data.put("Properties", GeneratorUtil.generateEntityProperties(columnInfos));
        data.put("Methods", GeneratorUtil.generateEntityMethods(columnInfos));

        String fileName = className + ".java";
        String fileName1 = className + "List.java";
        String fileName2 = className + "Search.java";
        String fileName3 = className + "Import.java";

        // 生成Entity文件
        FileUtil.generateFile(FreemarkerConfigUtils.TYPE_ENTITY, data, entityDir,fileName);
        FileUtil.generateFile(FreemarkerConfigUtils.TYPE_ENTITY_LIST, data, entityDir,fileName1);
        FileUtil.generateFile(FreemarkerConfigUtils.TYPE_ENTITY_SEARCH, data, entityDir,fileName2);
        FileUtil.generateFile(FreemarkerConfigUtils.TYPE_ENTITY_IMPORT, data, entityDir,fileName3);
    }
}
