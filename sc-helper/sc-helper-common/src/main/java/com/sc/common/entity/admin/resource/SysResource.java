package com.sc.common.entity.admin.resource;

import com.sc.common.entity.BaseEntity;

/**
 * Created by wust on 2019/4/28.
 */
public class SysResource extends BaseEntity {
    private static final long serialVersionUID = -1003092893683288911L;
    private String code;
    private String menuCode;	    //菜单code
    private String name;			//资源名
    private String description;     //资源描述
    private String permission;	    //资源需要权限,如user:add
    private String url;             //资源url
    private String requestMethod;
    private String lan;        // 语言
    private String userType;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMenuCode() {
        return menuCode;
    }

    public void setMenuCode(String menuCode) {
        this.menuCode = menuCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getRequestMethod() {
        return requestMethod;
    }

    public void setRequestMethod(String requestMethod) {
        this.requestMethod = requestMethod;
    }

    public String getLan() {
        return lan;
    }

    public void setLan(String lan) {
        this.lan = lan;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    @Override
    public String toString() {
        return "SysResource{" +
                "code='" + code + '\'' +
                ", menuCode='" + menuCode + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", permission='" + permission + '\'' +
                ", url='" + url + '\'' +
                ", requestMethod='" + requestMethod + '\'' +
                ", lan='" + lan + '\'' +
                ", userType='" + userType + '\'' +
                '}';
    }
}
