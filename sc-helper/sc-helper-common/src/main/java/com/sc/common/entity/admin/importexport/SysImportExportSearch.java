package com.sc.common.entity.admin.importexport;

import com.sc.common.dto.PageDto;

/**
 * Created by wust on 2019/5/20.
 */
public class SysImportExportSearch extends SysImportExport {

    private static final long serialVersionUID = -8936939623931148493L;
    private PageDto pageDto;

    public PageDto getPageDto() {
        return pageDto;
    }

    public void setPageDto(PageDto pageDto) {
        this.pageDto = pageDto;
    }
}
