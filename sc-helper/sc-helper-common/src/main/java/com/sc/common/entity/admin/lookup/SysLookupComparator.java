package com.sc.common.entity.admin.lookup;

import java.util.Comparator;

public class SysLookupComparator implements Comparator<SysLookup> {
    @Override
    public int compare(SysLookup o1, SysLookup o2) {
        if (o1.getSort().intValue() == o2.getSort().intValue()) {
            return 0;
        } else if (o1.getSort().intValue() > o2.getSort().intValue()) {
            return 1;
        } else {
            return -1;
        }
    }
}
