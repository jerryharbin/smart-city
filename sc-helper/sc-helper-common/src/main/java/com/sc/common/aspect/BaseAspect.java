package com.sc.common.aspect;

import cn.hutool.core.util.ReflectUtil;
import org.aspectj.lang.JoinPoint;
import org.springframework.aop.support.AopUtils;
import sun.reflect.generics.reflectiveObjects.ParameterizedTypeImpl;
import sun.reflect.generics.repository.ClassRepository;

import java.lang.reflect.Type;

public abstract class BaseAspect {
    protected Class getProxyClass(JoinPoint jp){
        Type[] types = AopUtils.getTargetClass(jp.getTarget()).getGenericInterfaces();
        Class type = (Class) types[0];
        return type;
    }

    protected Class getGenericClass(Class proxyClass){
        ClassRepository repository = (ClassRepository) ReflectUtil.getFieldValue(proxyClass,"genericInfo");
        Type[] types1 = (Type[]) ReflectUtil.getFieldValue(repository,"superInterfaces");
        ParameterizedTypeImpl parameterizedType = (ParameterizedTypeImpl)types1[0];
        Class returnClass = (Class)parameterizedType.getActualTypeArguments()[0];
        return returnClass;
    }
}
