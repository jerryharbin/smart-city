package com.sc.common;

import com.sc.common.enums.ApplicationEnum;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.RequestParameterBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ParameterType;
import springfox.documentation.service.RequestParameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import java.util.ArrayList;
import java.util.List;

public abstract class BaseSwaggerConfig {
    public Docket createRestApi() {
        List<RequestParameter> pars = new ArrayList<RequestParameter>();
        RequestParameterBuilder requestParameterBuilder = new RequestParameterBuilder();


        RequestParameter requestParameter1 = requestParameterBuilder
                .in(ParameterType.HEADER)
                .name(ApplicationEnum.X_AUTH_TOKEN.getStringValue())
                .description("非开放接口专用参数（调用App接口和Web必填参数）：登陆成功后，服务端会返回一个令牌，请使用该令牌粘贴到此处。")
                .required(false).build();
        pars.add(requestParameter1);


        RequestParameter requestParameter2 = requestParameterBuilder
                .in(ParameterType.HEADER)
                .name("appId")
                .description("开放接口专用参数（调用开放接口必填参数）：令牌标识，需要向服务接口提供方索取。")
                .required(false).build();
        pars.add(requestParameter2);

        RequestParameter requestParameter3 = requestParameterBuilder
                .in(ParameterType.HEADER)
                .name("sign")
                .description("开放接口专用参数（调用开放接口必填参数）：签名，根据相关规则生成，具体规则咨询接口提供方。")
                .required(false).build();
        pars.add(requestParameter3);

        RequestParameter requestParameter4 = requestParameterBuilder
                .in(ParameterType.HEADER)
                .name("nonce")
                .description("开放接口专用参数（调用开放接口必填参数）：请求唯一标识符。")
                .required(false).build();
        pars.add(requestParameter4);

        RequestParameter requestParameter5 = requestParameterBuilder
                .in(ParameterType.HEADER)
                .name("timestamp")
                .description("开放接口专用参数（调用开放接口必填参数）：时间戳，如1587518030174")
                .required(false).build();
        pars.add(requestParameter5);

        Docket docket = new Docket(DocumentationType.OAS_30)
                .groupName("admin-server")
                .enable(true)
                .apiInfo(defaultTitleInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage(getScanPackage()))
                .paths(PathSelectors.any())
                .build().globalRequestParameters(pars);
        return docket;
    }

    protected abstract String getScanPackage();

    protected abstract ApiInfo defaultTitleInfo();
}
