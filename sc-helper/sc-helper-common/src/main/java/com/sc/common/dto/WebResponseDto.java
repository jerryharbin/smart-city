package com.sc.common.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

@ApiModel(description = "Web接口返回对象")
public class WebResponseDto<T,E> implements Serializable {
    private static final long serialVersionUID = -4163766286277670113L;

    @ApiModelProperty(value = "成功标志",name = "SUCCESS",example = "SUCCESS")
    public static final String INFO_SUCCESS = "SUCCESS";

    @ApiModelProperty(value = "失败标志",name = "ERROR",example = "ERROR")
    public static final String INFO_ERROR = "ERROR";

    @ApiModelProperty(value = "警告标志",name = "WARNING",example = "WARNING")
    public static final String INFO_WARNING = "WARNING";

    @ApiModelProperty(value = "令牌",name = "token",example = "token")
    private String token;

    @ApiModelProperty(value = "code",name = "code",example = "code")
    private String code;

    @ApiModelProperty(value = "结果标志",name = "flag",example = "SUCCESS")
    private String flag;

    @ApiModelProperty(value = "结果消息",name = "message",example = "系统异常")
    private String message;

    @ApiModelProperty(value = "主表数据集合",name = "lstDto",example = "")
    private List<T> lstDto;

    @ApiModelProperty(value = "明细表数据集合",name = "lstDetailDto",example = "")
    private List<E> lstDetailDto;

    @ApiModelProperty(value = "分页对象",name = "page",example = "")
    private PageDto page;

    private T t;

    private E e;

    @ApiModelProperty(value = "通用对象",name = "obj",example = "")
    private Object obj;

    private Map mapMessage;

    public WebResponseDto() {
        this.setFlag(WebResponseDto.INFO_SUCCESS);
        this.setMessage("成功");
    }

    public boolean isSuccess(){
        if(WebResponseDto.INFO_SUCCESS.equals(this.getFlag())){
            return true;
        }
        return false;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<T> getLstDto() {
        return lstDto;
    }

    public void setLstDto(List<T> lstDto) {
        this.lstDto = lstDto;
    }

    public List<E> getLstDetailDto() {
        return lstDetailDto;
    }

    public void setLstDetailDto(List<E> lstDetailDto) {
        this.lstDetailDto = lstDetailDto;
    }

    public PageDto getPage() {
        return page;
    }

    public void setPage(PageDto page) {
        this.page = page;
    }

    public T getT() {
        return t;
    }

    public void setT(T t) {
        this.t = t;
    }

    public E getE() {
        return e;
    }

    public void setE(E e) {
        this.e = e;
    }

    public Object getObj() {
        return obj;
    }

    public void setObj(Object obj) {
        this.obj = obj;
    }

    public Map getMapMessage() {
        return mapMessage;
    }

    public void setMapMessage(Map mapMessage) {
        this.mapMessage = mapMessage;
    }

    @Override
    public String toString() {
        return "ResponseDto{" +
                "token='" + token + '\'' +
                ", code='" + code + '\'' +
                ", flag='" + flag + '\'' +
                ", message='" + message + '\'' +
                ", lstDto=" + lstDto +
                ", lstDetailDto=" + lstDetailDto +
                ", page=" + page +
                ", t=" + t +
                ", e=" + e +
                ", obj=" + obj +
                ", mapMessage=" + mapMessage +
                '}';
    }
}
