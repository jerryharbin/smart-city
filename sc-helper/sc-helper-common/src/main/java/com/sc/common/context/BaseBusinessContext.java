package com.sc.common.context;


import java.util.Locale;

/**
 * Created by wust on 2019/5/6.
 */
public class BaseBusinessContext implements java.io.Serializable{
    private static final long serialVersionUID = -1610450866158217407L;

    protected Locale locale;
    protected String dataSourceId;
    protected String dbType; // 数据库类型
    protected Long agentId;   // 代理商
    protected Long parentCompanyId; // 总公司
    protected Long branchCompanyId; // 分公司
    protected Long projectId; // 项目


    public Locale getLocale() {
        if(this.locale == null){
            this.locale = Locale.getDefault();
        }
        return locale;
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    public String getDbType() {
        return dbType;
    }

    public void setDbType(String dbType) {
        this.dbType = dbType;
    }

    public String getDataSourceId() {
        return dataSourceId;
    }

    public void setDataSourceId(String dataSourceId) {
        this.dataSourceId = dataSourceId;
    }

    public Long getAgentId() {
        return agentId;
    }

    public void setAgentId(Long agentId) {
        this.agentId = agentId;
    }

    public Long getParentCompanyId() {
        return parentCompanyId;
    }

    public void setParentCompanyId(Long parentCompanyId) {
        this.parentCompanyId = parentCompanyId;
    }

    public Long getBranchCompanyId() {
        return branchCompanyId;
    }

    public void setBranchCompanyId(Long branchCompanyId) {
        this.branchCompanyId = branchCompanyId;
    }

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    @Override
    public String toString() {
        return "BaseBusinessContext{" +
                "locale=" + locale +
                ", dataSourceId='" + dataSourceId + '\'' +
                ", agentId=" + agentId +
                ", parentCompanyId=" + parentCompanyId +
                ", branchCompanyId=" + branchCompanyId +
                ", projectId=" + projectId +
                '}';
    }
}
