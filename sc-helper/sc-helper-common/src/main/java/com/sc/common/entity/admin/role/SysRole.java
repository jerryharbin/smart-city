package com.sc.common.entity.admin.role;

import com.sc.common.annotations.EnableDistributedCaching;
import com.sc.common.annotations.EnableLocalCaching;
import com.sc.common.entity.BaseEntity;

/**
 * Created by wust on 2019/4/28.
 */
@EnableLocalCaching
@EnableDistributedCaching
public class SysRole extends BaseEntity {
    private static final long serialVersionUID = -975543183118133890L;
    private String code;
    private String name;	        // 角色名称
    private String description;     // 角色描述
    private String status;		    // 启用状态
    private Long agentId;
    private Long parentCompanyId;
    private Long branchCompanyId;
    private Long projectId;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    public Long getAgentId() {
        return agentId;
    }

    public void setAgentId(Long agentId) {
        this.agentId = agentId;
    }

    public Long getParentCompanyId() {
        return parentCompanyId;
    }

    public void setParentCompanyId(Long parentCompanyId) {
        this.parentCompanyId = parentCompanyId;
    }

    public Long getBranchCompanyId() {
        return branchCompanyId;
    }

    public void setBranchCompanyId(Long branchCompanyId) {
        this.branchCompanyId = branchCompanyId;
    }

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    @Override
    public String toString() {
        return super.toString() + "\nSysRole{" +
                "code='" + code + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", status='" + status + '\'' +
                ", agentId=" + agentId +
                ", parentCompanyId=" + parentCompanyId +
                ", branchCompanyId=" + branchCompanyId +
                ", projectId=" + projectId +
                '}';
    }
}
