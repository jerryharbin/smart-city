package com.sc.order.entity.goods;

import com.sc.common.dto.PageDto;

/**
 * @author: wust
 * @date: 2020-07-15 13:54:06
 * @description:
 */
public class GoodsSearch extends Goods {
    private PageDto pageDto;

    public PageDto getPageDto() {
        return pageDto;
    }

    public void setPageDto(PageDto pageDto) {
        this.pageDto = pageDto;
    }
}