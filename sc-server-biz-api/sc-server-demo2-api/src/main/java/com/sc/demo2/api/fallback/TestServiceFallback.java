package com.sc.demo2.api.fallback;

import com.sc.common.dto.WebResponseDto;
import com.sc.demo2.api.TestService;
import com.sc.demo2.entity.test.Test2;
import org.springframework.stereotype.Component;

/**
 * 服务降级配置
 */
@Component
public class TestServiceFallback implements TestService {
    @Override
    public WebResponseDto select(String ctx, Test2 search) {
        WebResponseDto webResponseDto = new WebResponseDto();
        webResponseDto.setFlag(WebResponseDto.INFO_ERROR);
        webResponseDto.setMessage("服务异常");
        return webResponseDto;
    }
}
