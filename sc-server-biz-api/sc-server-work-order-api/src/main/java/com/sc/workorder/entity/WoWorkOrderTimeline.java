package com.sc.workorder.entity;

import com.sc.common.entity.BaseEntity;

/**
 * @author: wust
 * @date: 2020-04-09 13:13:37
 * @description:
 */
public class WoWorkOrderTimeline extends BaseEntity {
    private String workOrderNumber;
    private String type;
    private String description;
    private Long companyId;
    private Long projectId;


    public void setWorkOrderNumber(String workOrderNumber) {
        this.workOrderNumber = workOrderNumber;
    }

    public String getWorkOrderNumber() {
        return workOrderNumber;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public Long getProjectId() {
        return projectId;
    }

}