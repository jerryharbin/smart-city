package com.sc.workorder.entity;

import com.sc.common.dto.PageDto;

/**
 * @author: wust
 * @date: 2020-04-01 10:27:05
 * @description:
 */
public class WoWorkOrderSearch extends WoWorkOrder {
    private PageDto pageDto;

    public PageDto getPageDto() {
        return pageDto;
    }

    public void setPageDto(PageDto pageDto) {
        this.pageDto = pageDto;
    }
}