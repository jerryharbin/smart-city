package com.sc.autotask;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@EnableEurekaClient
@EnableTransactionManagement
@MapperScan(basePackages = {"com.sc.common.dao","com.sc.autotask.core.dao"})
@ComponentScan(basePackages  = {"com.sc"},
        excludeFilters = @ComponentScan.Filter(
                type = FilterType.ASSIGNABLE_TYPE,
                classes = {}))
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class AutoTaskServerApplication {
    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(AutoTaskServerApplication.class);
        app.setWebApplicationType(WebApplicationType.REACTIVE);
        SpringApplication.run(AutoTaskServerApplication.class, args);
    }
}
