package com.sc.businesslog.core.service.impl;



import com.sc.businesslog.core.dao.BlOperationLogMapper;
import com.sc.businesslog.core.service.BlOperationLogService;
import com.sc.businesslog.entity.BlOperationLog;
import com.sc.common.mapper.IBaseMapper;
import com.sc.common.service.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by wust on 2019/5/28.
 */
@Service("blOperationLogServiceImpl")
public class BlOperationLogServiceImpl extends BaseServiceImpl<BlOperationLog> implements BlOperationLogService {
    @Autowired
    private BlOperationLogMapper blOperationLogMapper;

    @Override
    protected IBaseMapper getBaseMapper() {
        return blOperationLogMapper;
    }
}
