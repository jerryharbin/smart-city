/**
 * Created by wust on 2019-10-30 18:02:26
 * Copyright © 2019 wust. All rights reserved.
 */
package com.sc.admin.core.api.open.user;


import com.sc.admin.core.service.SysAccountService;
import com.sc.common.annotations.OpenApi;
import com.sc.common.annotations.OperationLog;
import com.sc.common.context.DefaultBusinessContext;
import com.sc.common.dto.WebResponseDto;
import com.sc.common.entity.admin.account.SysAccount;
import com.sc.common.enums.ApplicationEnum;
import com.sc.common.enums.OperationLogEnum;
import com.sc.common.enums.RedisKeyEnum;
import com.sc.common.util.MyStringUtils;
import com.sc.common.util.RC4;
import com.sc.common.util.cache.SpringRedisTools;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.Date;

/**
 * @author: wust
 * @date: Created in 2019-10-30 18:02:26
 * @description: 员工注册
 *
 */
@Api(tags = {"开放接口~app员工注册"})
@OpenApi
@RequestMapping("/api/open/v1/AppUserRegisterOpenApi")
@RestController
public class AppUserRegisterOpenApi {

    @Autowired
    private SysAccountService sysAccountServiceImpl;

    @Autowired
    private SpringRedisTools springRedisTools;


    @OperationLog(moduleName= OperationLogEnum.MODULE_COMMON,businessName="员工注册",operationType= OperationLogEnum.Insert)
    @RequestMapping(value = "/register",method = RequestMethod.POST,produces ="application/json;charset=utf-8")
    public @ResponseBody
    WebResponseDto register(@RequestParam String loginName,
                            @RequestParam String password,
                            @RequestParam String name,
                            @RequestParam String verificationCode) {
        WebResponseDto responseDto = new WebResponseDto();

        DefaultBusinessContext ctx = DefaultBusinessContext.getContext();

        if(MyStringUtils.isBlank(loginName)){
            responseDto.setFlag(WebResponseDto.INFO_WARNING);
            responseDto.setMessage("注册失败，缺少登录账号");
            return responseDto;
        }

        if(MyStringUtils.isBlank(password)){
            responseDto.setFlag(WebResponseDto.INFO_WARNING);
            responseDto.setMessage("注册失败，缺少账号密码");
            return responseDto;
        }
        if(MyStringUtils.isBlank(verificationCode)){
            responseDto.setFlag(WebResponseDto.INFO_WARNING);
            responseDto.setMessage("注册失败，缺少验证码");
            return responseDto;
        }

        String key = String.format(RedisKeyEnum.REDIS_KEY_STRING_VERIFICATION_CODE.getStringValue(),"1",loginName,verificationCode);
        if(!springRedisTools.hasKey(key)){
            responseDto.setFlag(WebResponseDto.INFO_WARNING);
            responseDto.setMessage("注册失败，无效的验证码");
            return responseDto;
        }
        springRedisTools.deleteByKey(key);


        String passwordRC4 = RC4.encry_RC4_string(password.toUpperCase(), ApplicationEnum.LOGIN_RC4_KEY.getStringValue());
        SysAccount account = new SysAccount();
        account.setAccountCode(loginName);
        account.setAccountName(name);
        account.setType("A101703"); // 员工账号
        account.setPassword(passwordRC4);
        account.setSource("开放接口注册员工");
        account.setCreaterId(ctx.getAccountId());
        account.setCreaterName(ctx.getAccountName());
        account.setCreateTime(new Date());
        sysAccountServiceImpl.insert(account);
        return responseDto;
    }
}
