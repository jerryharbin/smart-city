package com.sc.admin.core.service.impl;


import com.sc.admin.core.service.SysMenuService;
import com.sc.admin.core.dao.SysMenuMapper;
import com.sc.admin.core.dao.SysResourceMapper;
import com.sc.admin.core.dao.SysRoleResourceMapper;
import com.sc.common.mapper.IBaseMapper;
import com.sc.common.dto.WebResponseDto;
import com.sc.common.entity.admin.menu.SysMenu;
import com.sc.common.service.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.*;

/**
 * Created by wust on 2019/4/29.
 */
@Service("sysMenuServiceImpl")
public class SysMenuServiceImpl  extends BaseServiceImpl<SysMenu> implements SysMenuService {
    @Autowired
    private SysMenuMapper sysMenuMapper;

    @Autowired
    private SysResourceMapper sysResourceMapper;

    @Autowired
    private SysRoleResourceMapper sysRoleResourceMapper;


    @Override
    protected IBaseMapper getBaseMapper() {
        return sysMenuMapper;
    }



    @Override
    public List<SysMenu> findAsideMenu4superAdmin(String type,String lang) {
        return sysMenuMapper.findAsideMenu4superAdmin(type,lang);
    }

    @Override
    public List<SysMenu> findAsideMenu4admin(String permissionType, String lang, Long accountId) {
        return sysMenuMapper.findAsideMenu4admin(permissionType,lang,accountId);
    }

    @Override
    public List<SysMenu> findAsideMenuByUserId(String type,String lang,Long userId) {
        return sysMenuMapper.findAsideMenuByUserId(type,lang,userId);
    }


    @Override
    public List<SysMenu> findOneLevelMenus4superAdmin(String permissionType,String lang) {
        return sysMenuMapper.findOneLevelMenus4superAdmin(permissionType,lang);
    }

    @Override
    public List<SysMenu> findOneLevelMenus4admin(String permissionType, String lang, Long accountId) {
        return sysMenuMapper.findOneLevelMenus4admin(permissionType,lang,accountId);
    }

    @Override
    public List<SysMenu> findOneLevelMenusByUserId(String permissionType,String lang,Long userId) {
        return sysMenuMapper.findOneLevelMenusByUserId(permissionType,lang,userId);
    }

    @Override
    public List<SysMenu> findMenuNameByPermissionType(String permissionType, String lang) {
        return sysMenuMapper.findMenuNameByPermissionType(permissionType,lang);
    }

    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto create(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        return responseDto;
    }

    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto update(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        return responseDto;
    }

    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto delete(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        return responseDto;
    }
}
