package com.sc.admin.core.service.impl;

import com.sc.admin.core.dao.SysAccountResourceMapper;
import com.sc.admin.core.service.SysAccountResourceService;
import com.sc.common.mapper.IBaseMapper;
import com.sc.common.entity.admin.account.resource.SysAccountResource;
import com.sc.common.service.BaseServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import com.sc.common.dto.WebResponseDto;

/**
 * @author: wust
 * @date: 2020-03-26 09:45:35
 * @description:
 */
@Service("sysAccountResourceServiceImpl")
public class SysAccountResourceServiceImpl extends BaseServiceImpl<SysAccountResource> implements SysAccountResourceService {
    @Autowired
    private SysAccountResourceMapper sysAccountResourceMapper;

    @Override
    protected IBaseMapper getBaseMapper() {
        return sysAccountResourceMapper;
    }


    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto create(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        return responseDto;
    }


    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto update(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        return responseDto;
    }

    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto delete(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        return responseDto;
    }
}
