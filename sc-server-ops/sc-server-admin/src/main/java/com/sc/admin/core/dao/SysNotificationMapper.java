package com.sc.admin.core.dao;

import com.sc.common.mapper.IBaseMapper;
import com.sc.common.entity.admin.notification.SysNotification;

public interface SysNotificationMapper extends IBaseMapper<SysNotification> {
}