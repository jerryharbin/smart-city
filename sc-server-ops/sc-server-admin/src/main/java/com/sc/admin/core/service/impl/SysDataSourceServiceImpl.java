package com.sc.admin.core.service.impl;



import com.sc.admin.core.dao.SysDataSourceMapper;
import com.sc.admin.core.service.SysDataSourceService;
import com.sc.common.mapper.IBaseMapper;
import com.sc.common.dto.WebResponseDto;
import com.sc.common.entity.admin.datasource.SysDataSource;
import com.sc.common.service.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by wust on 2019/6/17.
 */
@Service("sysDataSourceServiceImpl")
public class SysDataSourceServiceImpl extends BaseServiceImpl<SysDataSource> implements SysDataSourceService {

    @Autowired
    private SysDataSourceMapper sysDataSourceMapper;



    @Override
    protected IBaseMapper getBaseMapper() {
        return sysDataSourceMapper;
    }

    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto create(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        return responseDto;
    }

    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto update(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        return responseDto;
    }

    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto delete(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        return responseDto;
    }
}
