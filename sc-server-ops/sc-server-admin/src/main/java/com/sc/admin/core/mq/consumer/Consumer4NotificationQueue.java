/**
 * Created by wust on 2019-11-09 14:00:06
 * Copyright © 2019 wust. All rights reserved.
 */
package com.sc.admin.core.mq.consumer;

import com.alibaba.fastjson.JSONObject;
import com.sc.admin.core.service.SysNotificationService;
import com.sc.common.entity.admin.notification.SysNotification;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.amqp.rabbit.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author: wust
 * @date: Created in 2019-11-09 14:00:06
 * @description: 通知消费队列
 *
 */
@Component
@RabbitListener(
        containerFactory = "singleListenerContainer",
        bindings = @QueueBinding(
                value = @Queue(
                        value = "${spring.rabbitmq.notification.queue.name}",
                        durable = "${spring.rabbitmq.notification.queue.durable}"
                ),
                exchange = @Exchange(
                        value = "${spring.rabbitmq.notification.exchange.name}",
                        durable = "${spring.rabbitmq.notification.exchange.durable}",
                        type = "${spring.rabbitmq.notification.exchange.type}",
                        ignoreDeclarationExceptions = "${spring.rabbitmq.notification.exchange.ignoreDeclarationExceptions}"
                ),
                key = "${spring.rabbitmq.notification.routing-key}"
        )
)
public class Consumer4NotificationQueue {
    static Logger logger = LogManager.getLogger(Consumer4NotificationQueue.class);
    @Autowired
    private SysNotificationService sysNotificationServiceImpl;

    @RabbitHandler
    public void process(JSONObject jsonObject) {
        logger.info("发布新通知消费队列,{}",jsonObject);
        SysNotification entity = jsonObject.getObject("entity",SysNotification.class);
        sysNotificationServiceImpl.insert(entity);
    }
}
