package com.sc.order.core.dao;

import com.sc.common.mapper.IBaseMapper;
import com.sc.order.entity.goods.Goods;

/**
 * @author: wust
 * @date: 2020-07-15 13:54:05
 * @description:
 */
public interface GoodsMapper extends IBaseMapper<Goods>{
}