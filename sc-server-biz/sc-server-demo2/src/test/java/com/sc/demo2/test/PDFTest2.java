package com.sc.demo2.test;


import com.sc.demo2.common.util.PDFUtil;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * 使用freemarker生成html，然后使用html生成pdf，最后对pdf进行签名
 */
public class PDFTest2 {
    public static void main(String[] args) throws Exception {
        // freemarker模板文件名
        String ftlFileName = "pdf.ftl";

        // freemarker模板目录
        String ftlFileDir = PDFUtil.getResourcePath4test() + "contract\\ftl";

        // 根据freemarker模板生成的html位置
        String htmlFilePath = PDFUtil.getResourcePath4test() + "contract\\html\\pdf.html";

        // 根据html成的pdf存储目录
        String pdfFilePath = PDFUtil.getResourcePath4test() + "contract\\pdf\\template" + System.currentTimeMillis() + ".pdf";

        // 加入印章并签名的pdf存储目录
        String pdfSignFilePath = PDFUtil.getResourcePath4test() + "contract\\pdf\\template" + System.currentTimeMillis() + "sign.pdf";

        // 加入手写签名的pdf存储目录
        String pdfSignFilePath1 = PDFUtil.getResourcePath4test() + "contract\\pdf\\template" + System.currentTimeMillis() + "sign.pdf";

        // 电子印章图片
        String sealFilePath = PDFUtil.getResourcePath4test() + "contract\\seal.gif";

        // 手写签名图片
        String signPngFilePath = PDFUtil.getResourcePath4test() + "contract\\signature.png";

        // 证书位置
        String p12FilePath = PDFUtil.getResourcePath4test() + "contract\\wust.p12";

        String pwd = "111111";

        Map data = new HashMap<>();
        data.put("companyFullName","广西XXX技术有限公司");
        data.put("contractNo","HT899u99");
        data.put("customerName","张三");


        // 生成PDF
        PDFUtil.createPDF(pdfFilePath,ftlFileDir,htmlFilePath,ftlFileName,595.0F,842.0F,data);


        // 为PDF加入电子印章
        PDFUtil.sign(new FileInputStream(pdfFilePath),
                new FileOutputStream(pdfSignFilePath),
                new FileInputStream(p12FilePath),
                pwd,
                "电子印章，不可更改",
                "平台",
                sealFilePath,
                300,
                600,
                630,
                500,
                1,
                "signature1");




        // 为PDF加入手写签名
        PDFUtil.sign(new FileInputStream(pdfSignFilePath),
                new FileOutputStream(pdfSignFilePath1),
                new FileInputStream(p12FilePath),
                pwd,
                "电子手写签名，不可更改",
                "平台",
                signPngFilePath,
                300,
                500,
                630,
                400,
                1,
                "signature2");
    }
}
